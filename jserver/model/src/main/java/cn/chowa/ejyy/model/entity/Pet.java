package cn.chowa.ejyy.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ejyy_pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("wechat_mp_user_id")
    private long wechatMpUserId;

    @JsonProperty("community_id")
    private long communityId;

    /**
     * 1 狗
     */
    @JsonProperty("pet_type")
    private int petType;

    private String name;

    /**
     * 1 公 0 母
     */
    private int sex;

    private String photo;

    /**
     * 毛色
     */
    @JsonProperty("coat_color")
    private String coatColor;

    private String breed;

    @JsonProperty("pet_license")
    private String petLicense;

    @JsonProperty("pet_license_award_at")
    private Long petLicenseAwardAt;

    private int remove;

    @JsonProperty("remove_reason")
    private int removeReason;

    @JsonProperty("removed_at")
    private Long removedAt;

    @JsonProperty("created_at")
    private long createdAt;

}
