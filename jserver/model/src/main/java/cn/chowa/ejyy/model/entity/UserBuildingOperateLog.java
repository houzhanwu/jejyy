package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 房产操作记录
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_user_building_operate_log")
public class UserBuildingOperateLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("user_building_id")
    private long userBuildingId;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("property_company_user_id")
    private Long propertyCompanyUserId;

    /**
     * 1 解绑；0 绑定
     */
    private int status;

    /**
     * 1 用户 2家人 3物业公司
     */
    @JsonProperty("operate_by")
    private int operateBy;

    @JsonProperty("created_at")
    private long createdAt;

}
