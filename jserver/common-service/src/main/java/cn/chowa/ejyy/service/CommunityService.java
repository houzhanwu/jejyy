package cn.chowa.ejyy.service;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.dao.UserDefaultCommunityRepository;
import cn.chowa.ejyy.model.entity.UserDefaultCommunity;
import cn.hutool.core.collection.ListUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.building.*;
import static cn.chowa.ejyy.common.Constants.status.BINDING_BUILDING;

@Service
public class CommunityService {

    @Autowired
    private BuildingQuery buildingQuery;
    @Autowired
    private UserDefaultCommunityRepository userDefaultCommunityRepository;

    public ObjData communityService(long wechatMpUserId) {
        List<ObjData> result = buildingQuery.getWechatCommunityUserBuilding(BINDING_BUILDING, wechatMpUserId);

        Map<Long, Community> map = new HashMap<>();
        result.forEach((ObjData record) -> {
            Long community_id = record.getLong("community_id");
            if (!map.containsKey(community_id)) {
                map.put(community_id,
                        Community.builder()
                                .community_id(community_id)
                                .name(record.getStr("name"))
                                .banner(record.getStr("banner"))
                                .phone(record.getStr("phone"))
                                .province(record.getStr("province"))
                                .city(record.getStr("city"))
                                .district(record.getStr("district"))
                                .houses(new ArrayList<>())
                                .carports(new ArrayList<>())
                                .warehouses(new ArrayList<>())
                                .merchants(new ArrayList<>())
                                .garages(new ArrayList<>())
                                .access_nfc(record.getInt("access_nfc"))
                                .access_qrcode(record.getInt("access_qrcode"))
                                .access_remote(record.getInt("access_remote"))
                                .fitment_pledge(record.getInt("fitment_pledge"))
                                .build());
            }

            int type = record.getInt("type");
            ObjData building = new ObjData(Map.of(
                    "building_id", record.getLong("building_id"),
                    "type", type,
                    "area", record.getStr("area"),
                    "building", record.getStr("building"),
                    "unit", record.getStr("unit"),
                    "number", record.getStr("number"),
                    "user_building_id", record.getLong("user_building_id"),
                    "authenticated", record.get("authenticated"),
                    "authenticated_type", record.getInt("authenticated_type")
            ));

            switch (type) {
                case HOUSE:
                    map.get(community_id).getHouses().add(building);
                    break;
                case CARPORT:
                    map.get(community_id).carports.add(building);
                    break;
                case WAREHOUSE:
                    map.get(community_id).warehouses.add(building);
                    break;
                case MERCHANT:
                    map.get(community_id).merchants.add(building);
                    break;
                case GARAGE:
                    map.get(community_id).garages.add(building);
                    break;
            }


        });

        List<Community> list = new ArrayList<>(map.values());
        ListUtil.reverse(list);

        UserDefaultCommunity mainCommunityInfo = userDefaultCommunityRepository.findByWechatMpUserId(wechatMpUserId);
        Long default_community_id = null;
        if (
                mainCommunityInfo != null &&
                        mainCommunityInfo.getCommunity_id() != null &&
                        list.stream().anyMatch(item -> item.getCommunity_id() == mainCommunityInfo.getCommunity_id())
        ) {
            default_community_id = mainCommunityInfo.getCommunity_id();
        } else {
            if (list.size() > 0) {
                default_community_id = list.get(0).getCommunity_id();

                if (mainCommunityInfo == null) {
                    userDefaultCommunityRepository.save(
                            UserDefaultCommunity.builder()
                                    .wechatMpUserId(wechatMpUserId)
                                    .community_id(default_community_id)
                                    .build()
                    );
                } else {
                    mainCommunityInfo.setCommunity_id(default_community_id);
                    userDefaultCommunityRepository.save(mainCommunityInfo);
                }
            }
        }

        Community current = new Community();

        if (default_community_id != null) {
            Long finalDefault_community_id = default_community_id;
            current = list.stream().takeWhile(c -> c.getCommunity_id() == finalDefault_community_id).findFirst().get();
        }

        return new ObjData(
                Map.of(
                        "list", list,
                        "current", current
                )
        );
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class Community {
        private long community_id;
        private String name;
        private String banner;
        private String phone;
        private String province;
        private String city;
        private String district;
        private List<ObjData> houses;
        private List<ObjData> carports;
        private List<ObjData> warehouses;
        private List<ObjData> merchants;
        private List<ObjData> garages;
        private int access_nfc;
        private int access_qrcode;
        private int access_remote;
        private int fitment_pledge;
    }

}
