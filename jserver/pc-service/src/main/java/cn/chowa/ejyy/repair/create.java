package cn.chowa.ejyy.repair;

import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.RepairRepository;
import cn.chowa.ejyy.model.entity.Repair;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static cn.chowa.ejyy.common.Constants.repair.SUBMIT_REPAIR_STEP;

@RestController("RepairCreate")
@RequestMapping("/pc/repair")
public class create {

    @Autowired
    private RepairRepository repairRepository;

    /**
     * 维修 - 新增
     */
    @SaCheckRole(Constants.RoleName.WXWF)
    @VerifyCommunity(true)
    @PostMapping("/create")
    public Map<?, ?> create(@RequestBody RequestData data) {
        Long wechat_mp_user_id = data.getLong("wechat_mp_user_id", false, "^\\d+$");
        int repair_type = data.getInt("repair_type", true, "^1|2|3|4$");
        int building_id = data.getInt("building_id", true, "^\\d+$");
        String description = data.getStr("description", true, 5, 200);
        List<String> repair_imgs = data.getArray("repair_imgs",
                (s -> Pattern.matches("^\\/repair\\/[a-z0-9]{32}\\.(jpg|jpeg|png)$", s)));

        Repair repair = repairRepository.save(Repair.builder()
                .propertyCompanyUserId(wechat_mp_user_id == null ? null : AuthUtil.getUid())
                .wechatMpUserId(wechat_mp_user_id)
                .communityId(data.getCommunityId())
                .repairType(repair_type)
                .buildingId(building_id)
                .description(description)
                .repairImgs(repair_imgs == null ? null : String.join("#", repair_imgs))
                .step(SUBMIT_REPAIR_STEP)
                .createdAt(System.currentTimeMillis())
                .build());

        return Map.of("id", repair.getId());
    }

}
