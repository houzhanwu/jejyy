package cn.chowa.ejyy.building;

import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.BuildingInfoRepository;
import cn.chowa.ejyy.dao.BuildingQuery;
import cn.chowa.ejyy.model.entity.BuildingInfo;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/pc/building")
public class history {

    @Autowired
    private BuildingInfoRepository buildingInfoRepository;
    @Autowired
    private BuildingQuery buildingQuery;

    /**
     * 房产历史操作记录
     */
    @SaCheckRole(Constants.RoleName.FCDA)
    @VerifyCommunity(true)
    @PostMapping("/history")
    public Map<?, ?> getHistory(@RequestBody RequestData data) {
        int id = (int) data.getId();
        Optional<BuildingInfo> optBuildingInfo = buildingInfoRepository.findById(id);
        if (optBuildingInfo.isEmpty()) {
            throw new CodeException(Constants.code.QUERY_ILLEFAL, "非法查询固定资产历史操作");
        }
        return Map.of(
                "list", buildingQuery.getBuildingHistoryOperations(id)
        );
    }


}
