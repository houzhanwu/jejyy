package cn.chowa.ejyy.car;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.UserCarQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;

@RestController("carDetail")
@RequestMapping("/pc/car")
public class detail {

    @Autowired
    private UserCarQuery userCarQuery;

    /**
     * 车辆详情
     */
    @SaCheckRole(Constants.RoleName.CLGL)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<String, Object> detail(@RequestBody RequestData data) {
        int id = data.getInt("id", true, "^\\d+$");

        ObjData info = userCarQuery.getUserCarInfo(id, data.getCommunityId());
        if (info == null) {
            throw new CodeException(QUERY_ILLEFAL, "非法获取车辆信息");
        }

        List<ObjData> operateList = userCarQuery.getUserCarOperateLogs(id);

        return Map.of(
                "info", info,
                "operateList", operateList
        );
    }

}
