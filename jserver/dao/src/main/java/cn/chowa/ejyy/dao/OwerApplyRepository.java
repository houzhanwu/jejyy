package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.OwerApply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * @ClassName OwerApplyRepository
 * @Description TODO
 * @Author ironman
 * @Date 15:18 2022/8/24
 */
public interface OwerApplyRepository extends JpaRepository<OwerApply,Long> {
    OwerApply findByIdAndCommunityId(long id, long communityId);
}
