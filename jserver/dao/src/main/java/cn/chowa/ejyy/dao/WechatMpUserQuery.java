package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

@JqlQuery
public interface WechatMpUserQuery {

    ObjData getWechatMpUserInfo(String openid);

    ObjData getWechatMapUserInfoById(int id);

}
