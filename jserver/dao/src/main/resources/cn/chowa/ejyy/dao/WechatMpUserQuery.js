function getWechatMpUserInfo(openid){
    return `
        select
            a.id,
            a.nick_name,
            a.phone,
            a.gender,
            a.avatar_url,
            a.signature,
            a.intact,
            a.created_at,
            b.subscribed
        from ejyy_wechat_mp_user a left join ejyy_wechat_official_accounts_user b on a.union_id=b.union_id
        where a.open_id=:openid
    `;
}

function getWechatMapUserInfoById(id) {
    var sql = `
    SELECT
        a.id,
        a.nick_name,
        a.real_name,
        a.idcard,
        a.phone,
        a.avatar_url,
        a.signature,
        a.gender,
        a.intact,
        a.created_at,
        b.subscribed
    FROM
    ejyy_wechat_mp_user a
    LEFT JOIN ejyy_wechat_official_accounts_user b ON b.union_id = a.union_id
    WHERE
    a.id =:id
    LIMIT 0,1;
    `;
    return sql;
}