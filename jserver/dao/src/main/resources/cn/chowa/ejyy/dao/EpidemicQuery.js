function getBuildingEpidemics(tour_code, return_hometown, community_id, page_size, page_num){
    var where="1=1 and ";
    where+=tour_code?"tour_code=:tour_code and ":"";
    where+=return_hometown?"return_hometown=:return_hometown and":"";

    var sql=`
        select
            a.id,a.tour_code,a.temperature,a.return_hometown,a.created_at,
            b.type,b.area,b.building,b.unit,b.number
        from ejyy_epidemic a left join ejyy_building_info b on a.building_id=b.id
        where ${where} a.community_id=:community_id
        order by a.id desc
        limit ${(page_num-1)*page_size},${page_size}
    `;

    return sql;
}

function getBuildingEpidemicsCount(tour_code, return_hometown, community_id){
    var where="1=1 and ";
    where+=tour_code?"tour_code=:tour_code and ":"";
    where+=return_hometown?"return_hometown=:return_hometown and":"";

    var sql=`
        select count(*)
        from ejyy_epidemic a left join ejyy_building_info b on a.building_id=b.id
        where ${where} a.community_id=:community_id
    `;

    return sql;
}

function getEpidemicDetail( id,  community_id){
    var sql=`
        select
            a.id,
            a.building_id,
            a.tour_code,
            a.wechat_mp_user_id,
            a.temperature,
            a.return_hometown,
            a.return_from_province,
            a.return_from_city,
            a.return_from_district,
            a.created_by,
            a.created_at,
            b.type,
            b.area,
            b.building,
            b.unit,
            b.number,
            c.real_name as user_real_name,
            d.real_name as created_user_real_name
        from ejyy_epidemic a left join ejyy_building_info b on a.building_id=b.id
        left join ejyy_wechat_mp_user c on a.wechat_mp_user_id=c.id
        left join ejyy_property_company_user d on a.created_by=d.id
        where a.id=:id and a.community_id=:community_id
    `;

    return sql;
}